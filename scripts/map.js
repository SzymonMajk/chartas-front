function inflate() {
    let files = document.getElementById('inflateFile').files;

    if (files.length <= 0) {
        let startDate = document.getElementById('start').value;
        let endDate = document.getElementById('end').value;
        let threshold = document.getElementById('threshold').value;
        if (isNotCorrectDate(startDate) || isNotCorrectDate(endDate)) {
            alert("Wrong date format remember to use yyyy-mm-dd");
            return false;
        }

        fetch("http://" + localStorage.getItem("hostname") +
            "exporters/geojson?start=" + startDate + "&end=" + endDate + "&threshold=" + threshold)
            .then(
                function(response) {
                    if (response.status !== 200) {
                        console.log('Status code problem: ' + response.status);
                        return;
                    }

                    return response.json()
                }
            )
            .then(function(data) {
                console.log(data);
                show(data);
            });

        return false;
    }

    let fr = new FileReader();

    fr.onload = function (e) {
        show(JSON.parse(e.target.result));
    };

    fr.readAsText(files.item(0));
    return false;
}

function show(geojson) {
    let mymap = L.map('mapid').setView([51.505, -0.09], 13);

    L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Terrain_Base/MapServer/tile/{z}/{y}/{x}', {
        attribution: 'Tiles &copy; Esri &mdash; Source: USGS, Esri, TANA, DeLorme, and NPS',
        maxZoom: 12,
        minZoom: 4
    }).addTo(mymap);

    L.geoJSON(geojson).addTo(mymap);

    mymap.on('click', function(e) {
        alert("You clicked the map at " + e.latlng);
    });
}

function isNotCorrectDate(str) {
    let regEx = /^\d{4}-\d{2}-\d{2}$/;
    return str.match(regEx) == null;
}